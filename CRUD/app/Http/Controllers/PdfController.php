<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Anouar\Fpdf\Facades\Fpdf;
use PDF;

class PdfController extends Controller
{
   

    public function saludo() {
    $data=[
        'Titulo'=>'Factura',
        'Domicilio'=>'no23 calle tal',
        'Cliente'=>'Alguien',
        'Productos'=>[
            'Papel',
            'Cuaderno',
            'lapices',
            'goma'
        ],
        'Costo'=>[
            15,
            20,
            10,
            5,
        ],
        'NumeroFactura'=>'101010'
        
     ];
  
    $pdf = PDF::loadView('pdf.invoice',$data);
   return $pdf->stream('invoice.pdf');
       
    }
}
