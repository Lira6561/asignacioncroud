<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuario;

class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuarios= Usuario::orderBy('id','DESC')->paginate(3);
        return view('Usuario.index',compact('usuarios')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Usuario.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[ 'nombre'=>'required', 'correo'=>'required', 'contrasenia'=>'required', 'tipoUsuario'=>'required', 'estado'=>'required']);
        usuario::create($request->all());
        
        return redirect()->route('usuario.index')->with('success','Registro creado satisfactoriamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $usuarios= usuario::find($id);
        return view('usuario.show',compact('usuarios'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usuario=usuario::find($id);
        return view('usuario.edit',compact('usuario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $this->validate($request,[ 'nombre'=>'required', 'correo'=>'required', 'contrasenia'=>'required', 'tipoUsuario'=>'required', 'estado'=>'required']);
        
        usuario::find($id)->update($request->all());
        return redirect()->route('usuario.index')->with('success','Registro actualizado satisfactoriamente');
  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        usuario::find($id)->delete();
        return redirect()->route('usuario.index')->with('success','Registro eliminado satisfactoriamente');
    }
}
