<?php
namespace App\Model\Table;

use App\Model\Entity\User;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @property \App\Model\Table\BitacoraTable|\Cake\ORM\Association\HasMany $Bitacora
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 */
class UsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

    
      
        $this->setPrimaryKey('id');
        
        $this->hasMany('Bitacoras',[
            'foreignKey' => 'user_id'
        ]);
        
    
        $this->addBehavior('Proffer.Proffer', [
	'photo' => [	// The name of your upload field
		'root' => WWW_ROOT . 'files', // Customise the root upload folder here, or omit to use the default
		'dir' => 'photo_dir',	// The name of the field to store the folder
		'thumbnailSizes' => [ // Declare your thumbnails
			'square' => [	// Define the prefix of your thumbnail
				'w' => 200,	// Width
				'h' => 200,	// Height
                                'crop' => true,
				'jpeg_quality'	=> 100
			]
		],
		'thumbnailMethod' => 'gd'	// Options are Imagick or Gd
	]
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->allowEmptyString('email', false);

        $validator
            ->scalar('apellidopaterno')
            ->maxLength('apellidopaterno', 255)
            ->requirePresence('apellidopaterno', 'create')
            ->allowEmptyString('apellidopaterno', false)
            ->add('apellidopaterno', 'validFormat', [
                'rule' => ['custom', '/^[a-zA-ZñÑáéíóúÁÉÍÓÚ]+(?:[\s]+[a-zA-ZñÑáéíóúÁÉÍÓÚ]+)*$/'],
                'message' => 'Error en la validación de este campo']);;

        $validator
            ->scalar('apellidomaterno')
            ->maxLength('apellidomaterno', 255)
            ->requirePresence('apellidomaterno', 'create')
            ->allowEmptyString('apellidomaterno', false)
            ->add('apellidomaterno', 'validFormat', [
                'rule' => ['custom', '/^[a-zA-ZñÑáéíóúÁÉÍÓÚ]+(?:[\s]+[a-zA-ZñÑáéíóúÁÉÍÓÚ]+)*$/'],
                'message' => 'Error en la validación de este campo']);;
        

        $validator
            ->scalar('nombre')
            ->maxLength('nombre', 255)
            ->requirePresence('nombre', 'create')
            ->allowEmptyString('nombre', false)
            ->add('nombre', 'validFormat', [
                'rule' => ['custom', '/^[a-zA-ZñÑáéíóúÁÉÍÓÚ]+(?:[\s]+[a-zA-ZñÑáéíóúÁÉÍÓÚ]+)*$/'],
                'message' => 'Error en la validación de este campo']);

        $validator
            ->boolean('status')
            ->allowEmptyString('status');

        $validator
            ->boolean('tipo')
            ->allowEmptyString('tipo');

        $validator
            ->scalar('password')
            ->maxLength('password', 255)
            ->requirePresence('password', 'create')
            ->allowEmptyString('password', false);
        
        $validator
            // Cargamos el proveedor del plugin.
            ->provider('proffer', 'Proffer\Model\Validation\ProfferRules')
            // Set the thumbnail resize dimensions
            ->add('photo', 'proffer', [
                'rule' => ['dimensions', [
                    'min' => ['w' => 100, 'h' => 100],
                    'max' => ['w' => 1000, 'h' => 1000]
                ]],
                'message' => 'La imagen no tiene dimensión entre 100x100/1000x1000 pixeles.',
                'provider' => 'proffer',
            ])
            ->add('photo', 'extension', [
                'rule' => ['extension', [
                    'jpg', 'gif', 'png'
                ]],
                'message' => 'La imagen no tiene extensión correcta, ya sea jpg, gif o png.',
            ])
            ->add('photo', 'fileSize', [
                'rule' => ['fileSize', '<=', '1MB'],
                'message' => 'La imagen no debe exceder de 1MB de peso.',
            ])
            ->add('photo', 'mimeType', [
                'rule' => ['mimeType', ['image/jpeg', 'image/png']],
                'message' => 'La imagen no tiene un formato correcto.',
            ])
            ->requirePresence('photo', 'create')
            ->notEmpty('photo', null, 'create');
         
        
        



        return $validator;
    }
    
   
  
    public function validationPwd(Validator $validator)
    {
        $validator
           ->requirePresence('Nuevapwd', 'create')
            ->notEmpty('Nuevapwd')
            ->add('Nuevapwd','validFormat',[
                'rule' => ['custom', '/^([A-Za-z\d%&@#$^*¡!¿?_,.~]+)*$/'],
                'message' => 'Debe contener al menos una mayúscula, una minúscula, un número y un caracter especial.'])
            ->minLength('Nuevapwd', 8, 'La contraseña mínima es de 8 caracteres.')
            ->maxLength('Nuevapwd', 15, 'La contraseña máxima es de 15 caracteres.');
        

        return $validator;
    }
    
    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));

        return $rules;
    }
    
   
}
