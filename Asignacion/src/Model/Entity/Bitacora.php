<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Bitacora Entity
 *
 * @property int $b_id
 * @property int|null $user_id
 * @property string $email
 * @property string $accion
 * @property \Cake\I18n\FrozenTime|null $fecha
 *
 * @property \App\Model\Entity\User $user
 */
class Bitacora extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'email' => true,
        'accion' => true,
        'fecha' => true,
        'user' => true
    ];
}
