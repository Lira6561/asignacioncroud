<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Bitacoras Controller
 *
 * @property \App\Model\Table\BitacorasTable $Bitacoras
 *
 * @method \App\Model\Entity\Bitacora[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class BitacorasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $bitacoras = $this->paginate($this->Bitacoras);

        $this->set(compact('bitacoras'));
    }

    /**
     * View method
     *
     * @param string|null $id Bitacora id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $bitacora = $this->Bitacoras->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('bitacora', $bitacora);
    }

 
}
