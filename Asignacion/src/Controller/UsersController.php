<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Mailer\Email;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        
        $user = $this->Users->get($this->getRequest()->getSession()->read('Auth.User.id'));

        $this->set('user', $user);
        $users = $this->paginate($this->Users);
        $this->set(compact('users'));
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id);

        $this->set('user', $user);
        
        $bitacora= $this->Users->Bitacoras->find('list');
        $this->set('bitacora',$bitacora);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function GeneracionPwd( $length ) {

    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    return substr(str_shuffle($chars),0,$length);
    }



    public function add()
    {
        $user = $this->Users->newEntity();
      
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            
            
            $pwd=$this->GeneracionPwd(5);
            $user->password=$pwd;
            
            //exit;
            $email = new Email();
            $email->from(['dalies6561@gmail.com' => 'Asignacion Cakephp'])
            ->to($user->email)
       
            ->subject('Contraseña secreta')
            ->send('Hola, esta es tu contraseña: '. $pwd);
            
           
            $bitacora=$this->Users->Bitacoras->newEntity();
            $bitacora->accion='Agregó a usuario '.$user->email;
            $bitacora->fecha='now()';
            $bitacora->email=$this->getRequest()->getSession()->read('Auth.User.email');
            $bitacora->user_id=$this->getRequest()->getSession()->read('Auth.User.id');
          
            $this->Users->Bitacoras->save($bitacora);
            
            if ($this->Users->save($user)) {
                $this->Flash->success(__('El usuario ha sido guardado exitósamente.'));
                    
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El usuario no se pudo guardar, intente más tarde'));
        }
        $this->set(compact('user'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            
            
            $bitacora=$this->Users->Bitacoras->newEntity();
            $bitacora->accion='Editó a usuario '.$user->email;
            $bitacora->fecha='now()';
            $bitacora->email=$this->getRequest()->getSession()->read('Auth.User.email');
            $bitacora->user_id=$this->getRequest()->getSession()->read('Auth.User.id');
          
            $this->Users->Bitacoras->save($bitacora);
            
            if ($this->Users->save($user)) {
                $this->Flash->success(__('El usuario ha sido editado'));
                
              
              
                
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El usuario no puede ser editado, intentelo más tarde'));
        }
        $this->set(compact('user'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        
        $bitacora=$this->Users->Bitacoras->newEntity();
        $bitacora->accion='Eliminó a usuario '.$user->email;
        $bitacora->fecha='now()';
        $bitacora->email=$this->getRequest()->getSession()->read('Auth.User.email');
        $bitacora->user_id=$this->getRequest()->getSession()->read('Auth.User.id');
          
        $this->Users->Bitacoras->save($bitacora);
        
        
        
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('El usuario ha sido eliminado'));
        } else {
            $this->Flash->error(__('El usuario no puede ser eliminado, intentelo más tarde'));
        }

        return $this->redirect(['action' => 'index']);
    }
    
    public $paginate = [
        'limit' => 10,
        'order' => [
            'Usuarios.apellidopaterno' => 'asc'
        ]
    ];
    
   
    public function logout(){
        return $this->redirect($this->Auth->logout());
    }
    
    public function TipoUsuario($tipo){
        if($tipo){
                $home=$this->Auth->redirectUrl();
            }
            else{
                $home=array("action"=>"home");
            }
        return $home;
    }
    
    public function home(){
        
    }

    public function login()
    {
    if ($this->request->is('post')) {
        
        $recaptcha =new \ReCaptcha\ReCaptcha('6LcCRowUAAAAAG_koZQKEqGWbj-UZO37zV3ey4ml');
        //debug($this->request->data['g-recaptcha-response']);
        $reponse=$recaptcha->verify($this->request->data['g-recaptcha-response'],$this->request->clientIp());
       
            
        
        $user = $this->Auth->identify();
        if ($user && $reponse->isSuccess()) {
            $this->Auth->setUser($user);
             if(!$this->getRequest()->getSession()->read('Auth.User.status')){
                 $this->Flash->set('¡El usuario '.$this->getRequest()->getSession()->read('Auth.User.nombre').' se encuentra deshabilidado!');
                 return;
                 
             }    
            $this->Flash->set('¡Bienvenido '.$this->getRequest()->getSession()->read('Auth.User.nombre').'!');
            
            return $this->redirect($this->TipoUsuario($this->getRequest()->getSession()->read('Auth.User.tipo')));
        
            
        }else{
            
            $this->Flash->error('Tu correo,  contraseña o reCaptcha es incorrecto');
            
        }}
    }
    
    
    public function isAuthorized($user)
    {
  
    if (in_array($this->request->getParam('action'),['home','logout','cambio'])) {
        return true;
    }
    

    

    return parent::isAuthorized($user);
    }
    
    public function recuperacion(){
        if($this->request->is(['patch', 'post', 'put'])){
            
         $correo = $this->request->getData(['email']);
         
         $consulta= $this->Users
                 ->find()
                 ->select(['id','nombre'])
                 ->where(['email LIKE' => $correo]);
               
         $consu=$consulta->toArray();
         //debug($consu[0]['nombre']);
         
         
         //debug($user);
         
         if(!$consulta->toList()==null){
             
             $id=$consu[0]['id'];
             $user = $this->Users->get($id);
             
             $NuevaPwd=$this->GeneracionPwd(5);
             
             $user->password=$NuevaPwd;
             
             $this->Users->save($user);
             
             $bitacora=$this->Users->Bitacoras->newEntity();
             $bitacora->accion='Recuperó contraseña';
             $bitacora->fecha='now()';
             $bitacora->email=$user->email;
             $bitacora->user_id=$user->id;
          
             $this->Users->Bitacoras->save($bitacora);
             
                
             $email = new Email();
            $email->from(['dalies6561@gmail.com' => 'Asignacion Cakephp'])
            ->to($user->email)
            ->subject('Recuperación de contraseña')
            ->send('Hola, esta es tu nueva contraseña: '. $NuevaPwd);
             
            $this->Flash->success(__('Tu contraseña ha sido enviada al correo registrado'));
            
         }else{
             $this->Flash->set('Este usuario no existe');
       
        }
        }  
    }
    
    public function cambio(){
        
        if($this->request->is(['patch', 'post', 'put'])){
            
            $Validad= $this->Users->newEntity($this->request->getData(),['validate'=>'pwd']);
           
            
            $NuevaPwd = $this->request->getData(['Nuevapwd']);
            $NuevaPwd2 = $this->request->getData(['Nuevapwd2']);
            $coincidencia=true;
            
       
        
           
            if($NuevaPwd!=$NuevaPwd2){
                $coincidencia=false;
                $this->Flash->error('La nueva contraseña no coincide');
                return;
            }
           
            if($Validad->errors()){
               
                $this->Flash->error('La contraseña no cumple');
                return;
            }
            
            if(!$this->Auth->identify() == false && $coincidencia==true){
          
                $userId = $this->Users->get($this->getRequest()->getSession()->read('Auth.User.id'));
                
                
               
              
                $user = $this->Users->get($userId['id']);
                
                $user->password=$NuevaPwd;
                $this->Users->save($user);
                
                $this->Flash->success('Se ha cambiado contraseña');
                
                 $bitacora=$this->Users->Bitacoras->newEntity();
                 $bitacora->accion='Hizo cambio de contraseña.';
                 $bitacora->fecha='now()';
                 $bitacora->email=$this->getRequest()->getSession()->read('Auth.User.email');
                 $bitacora->user_id=$this->getRequest()->getSession()->read('Auth.User.id');
          
                 $this->Users->Bitacoras->save($bitacora);
                return $this->redirect(['action' => 'index']);
                
                
           }else{
                $this->Flash->error('Tu contraseña actual es incorrecta');
                
               
                
           }
            
           
        }
    }
    
    
   
}
