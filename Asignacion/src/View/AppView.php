<?php

namespace App\View;

use Cake\View\View;
use BootstrapUI\View\UIViewTrait;

class AppView extends View
{
    use UIViewTrait;
    
    public function initialize()
    {
        $this->initializeUI(['layout'=>false]);
    }
}
