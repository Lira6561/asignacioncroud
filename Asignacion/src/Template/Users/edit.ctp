<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<nav style="margin-top: 2em">
    <ul class="nav nav-tabs">
     
      
        <li><?= $this->Html->link(__('Lista Usuario'), ['action' => 'index']) ?></li>
        <li><?= $this->Form->postLink(
                __('Eliminar'),
                ['action' => 'delete', $user->id],
                ['confirm' => __('¿Estás seguro de elminar el usuario # {0}?', $user->id),'class'=>'btn btn-danger']
            )
        ?></li>
  
    </ul>
</nav>
<div style="margin-top: 2em">
    <?= $this->Form->create($user,['type' => 'file']) ?>
    <fieldset>
        <legend><?= __('Editar Usuario') ?></legend>
        <img id='output' width='20%'>
            <?php
            echo $this->Html->image('/files/users/photo/'.$user->photo_dir.'/'.$user->photo,['width'=>'20%']); 
            echo $this->Form->input('photo', ['label'=>'Imagen','type' => 'file','onchange'=>'openFile(event)']);
            echo $this->Form->control('email',['label'=>'Nombre']);
            echo $this->Form->control('apellidopaterno',['label'=>'Apellido Paterno']);
            echo $this->Form->control('apellidomaterno',['label'=>'Apellido Materno']);
            echo $this->Form->control('nombre',['label'=>'Nombre']);
            echo $this->Form->input('tipo',['options'=>[0=>'Usuario',1=>'Administrador'],'label'=>'Rol']);
            echo $this->Form->control('status',['label'=>'Activo']);
  
        ?>
    </fieldset>
    <?= $this->Form->button(__('Guardar'),array('controller'=>'Bitacoras','action'=>'add')) ?>
    <?= $this->Form->end() ?>
</div>
<script>
  var openFile = function(event) {
    var input = event.target;

    var reader = new FileReader();
    reader.onload = function(){
      var dataURL = reader.result;
      var output = document.getElementById('output');
      output.src = dataURL;
    };
    reader.readAsDataURL(input.files[0]);
  };
</script>