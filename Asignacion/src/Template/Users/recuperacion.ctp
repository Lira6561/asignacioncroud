<h1 style='margin-bottom: 1em'>Recuperación</h1>

<?= $this->Html->link(__('<Volver'), ['action' => 'login'],['class'=>'btn btn-sm btn-danger']) ?>
<div style='margin-top: 2em'>
    <label class='text-sm text-muted'>Te enviaremos de nuevo tu cotraseña por correo electrónico</label>
<?= $this->Form->create() ?>
<?= $this->Form->control('email',['label'=>'Correo electrónico']) ?>
<?= $this->Form->button('Enviar') ?>
<?= $this->Form->end() ?>
</div>