<h1 style='margin-bottom: 1em'>Cambio de contraseña</h1>

<?= $this->Html->link(__('<<Volver'), ['action' => 'index'],['class'=>'btn btn-sm btn-danger']) ?>
<div style='margin-top: 2em'>
    <label class='text-sm text-muted'>Introduce tu contraseña actual y tu nueva contraseña</label>
<?= $this->Form->create() ?>
<?= $this->Form->control('email',['type'=>'hidden','value'=>$this->getRequest()->getSession()->read('Auth.User.email')]) ?>
<?= $this->Form->control('password',['label'=>'Contraseña actual', 'type'=>'password','required'=>'required']) ?>
<?= $this->Form->control('Nuevapwd',['label'=>'Nueva contraseña', 'type'=>'password','required'=>'required']) ?>
<?= $this->Form->control('Nuevapwd2',['label'=>'Repetir nueva contraseña', 'type'=>'password','required'=>'required']) ?>
<?= $this->Form->button('Guardar') ?>
<?= $this->Form->end() ?>
</div>