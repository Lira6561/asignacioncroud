
<br>
<h1>Inicio de sesión</h1>

<?= $this->Form->create() ?>
<?= $this->Form->control('email',['label'=>'Correo']) ?>
<?= $this->Form->control('password',['label'=>'Contraseña']) ?>

<div style="margin-top: 1em" class="g-recaptcha" data-sitekey="6LcCRowUAAAAAJPC4KEX9bHT5xEI5elQqyVeZCcK"></div>
<br>
<div class="form-group row">  
    <div class='col-md-4 col-sm-9 col-xs-6'>
           <?= $this->Form->button('Acceder',['class'=>'btn btn-success ']) ?>
    </div>
    <div class='col-md-4 col-sm-3 col-xs-6'>
       <?= $this->Html->link(__('Recuperar contraseña'), ['action' => 'recuperacion'],['class'=>'btn btn-sm btn-info']) ?>
    </div>
</div>
<?= $this->Form->end() ?>
