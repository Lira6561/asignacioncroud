<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<nav style="margin-top: 2em">
    <ul class="nav nav-tabs">
       
        <li><?= $this->Html->link(__('Lista Usuario'), ['action' => 'index']) ?></li>
     
        <li><?= $this->Html->link(__('Nuevo usuario'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div style="margin-top: 2em">
    <?= $this->Form->create($user,['type' => 'file']) ?>
    <fieldset>
        <legend><?= __('Agregar Usuario') ?></legend>
        <p style="color: tomato">* Campos obligatorios</p>
        <?php
         
            echo $this->Form->input('photo', ['type' => 'file']);
            echo $this->Form->control('email',['label'=>'Correo electrónico*']);
            echo $this->Form->control('apellidopaterno',['label'=>'Apellido Paterno*']);
            echo $this->Form->control('apellidomaterno',['label'=>'Apellido Materno*']);
            echo $this->Form->control('nombre',['label'=>'Nombre*']);
            echo $this->Form->input('tipo',['options'=>[0=>'Usuario',1=>'Administrador'],'label'=>'Rol']);
            echo $this->Form->control('status',['label'=>'Activo']);
  
        ?>
    </fieldset>
    <?= $this->Form->button(__('Guardar')) ?>
    <?= $this->Form->end() ?>
</div>
