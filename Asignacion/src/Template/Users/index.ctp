<?php

?>
<nav style="margin-top: 2em" class=" row">
    <ul class="nav nav-tabs col-sm-9">
       
        <li ><?= $this->Html->link(__('Nuevo Usuario'), ['action' => 'add']) ?></li>
        <li ><?= $this->Html->link(__('Bitácora'), ['controller'=>'bitacoras', 'action' => 'index']) ?></li>
        <li ><?= $this->Html->link(__('Cambio de contraseña'), [ 'action' => 'cambio']) ?></li>
        <li >
            <?= $this->Html->link(__('Salir'),['controller'=>'Users',  'action'=>'logout'],['class'=>'btn btn-danger'])?>
        </li>
    
     
    </ul>
    <div class="col-sm-3">
        <?= $this->Html->image('/files/users/photo/'.$user->photo_dir.'/'.$user->photo,['width'=>'100%'])?>
        <?= $this->Html->link($user->email,[
            'action'=>'view',
            $user->id
        ],['class'=>'btn btn-sm'])?>
    </div>
</nav>
<div>
    <h3><?= __('Usuarios') ?></h3>
    <table cellpadding="0" cellspacing="0" class="table table-striped">
        <thead>
            <tr>
                
                
                <th scope="col"><?= $this->Paginator->sort('apellidopaterno',['Apellido Paterno']) ?></th>
                <th scope="col"><?= $this->Paginator->sort('apellidomaterno', ['Apellido Materno']) ?></th>
                <th scope="col"><?= $this->Paginator->sort('nombre',['Nombre']) ?></th>
                <th scope="col"><?= $this->Paginator->sort('email',['Correo']) ?></th>
                <th scope="col"><?= $this->Paginator->sort('status',['Estatus']) ?></th>
                <th scope="col"><?= $this->Paginator->sort('tipo',['Tipo']) ?></th>
              
                <th scope="col" class="actions"><?= __('Acciones') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($users as $user): ?>
            <tr>
               
               
                <td><?= h($user->apellidopaterno) ?></td>
                <td><?= h($user->apellidomaterno) ?></td>
                <td><?= h($user->nombre) ?></td>
                <td><?= h($user->email) ?></td>
                <td><?= h($user->status==0?'Deshabilitado':'Activo') ?></td>
                <td><?= h($user->tipo==0?'Usuario':'Administrador') ?></td>
               
                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['action' => 'view', $user->id],['class'=>'btn btn-sm btn-info','style'=>'width:5em']) ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $user->id],['class'=>'btn btn-sm btn-primary','style'=>'width:5em']) ?>
                    <?= $this->Form->postLink(__('Eliminar'), ['action' => 'delete', $user->id], ['confirm' => __('¿Estás seguro de eliminar este usuario # {0}?', $user->id), 'class'=>'btn btn-sm btn-danger','style'=>'width:5em']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
           
            <?= $this->Paginator->prev('< ' . __('Anterior')) ?>
            <?= $this->Paginator->numbers(['before'=> '','after'=>'']) ?>
            <?= $this->Paginator->next(__('Siguiente') . ' >') ?>
           
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Pagina {{page}} de {{pages}}')]) ?></p>
    </div>
</div>
