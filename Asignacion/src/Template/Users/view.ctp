<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<nav style='margin-top:2em' id="actions-sidebar">
    <ul class="nav nav-tabs" >
       
        <li><?= $this->Html->link(__('Editar Usuario'), ['action' => 'edit', $user->id]) ?> </li>
        
        <li><?= $this->Html->link(__('Lista Usuarios'), ['action' => 'index']) ?> </li>
          <li><?= $this->Form->postLink(__('Eliminar usuario'), ['action' => 'delete', $user->id], ['confirm' => __('¿Estás seguro de eliminar este usuario # {0}?', $user->id),'class'=>'btn btn-danger']) ?> </li>
      
    </ul>
</nav>
<div  style='margin-top:2em'>
    <h3><?= 'Ficha de usuario' ?></h3>
    <table class="table table-striped">
        <tr>
            <th scope="row"><?= __('Foto') ?></th>
            <td><?= $this->Html->image('/files/users/photo/'.$user->photo_dir.'/'.$user->photo,['width'=>'50%']) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Correo electrónico') ?></th>
            <td><?= h($user->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Apellido Paterno') ?></th>
            <td><?= h($user->apellidopaterno) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Apellido Materno') ?></th>
            <td><?= h($user->apellidomaterno) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Nombre') ?></th>
            <td><?= h($user->nombre) ?></td>
        </tr>
       
     
        <tr>
            <th scope="row"><?= __('Estatus') ?></th>
            <td><?= $user->status==0?'Deshabilitado':'Activo' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Tipo') ?></th>
            <td><?= $user->tipo==0?'Usuario':'Administrador' ?></td>
        </tr>
    </table>
    

   
</div>
