<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Bitacora[]|\Cake\Collection\CollectionInterface $bitacoras
 */
?>
<nav style="margin-top: 2em" id="actions-sidebar">
    <ul class="nav nav-tabs">
      
        <li><?= $this->Html->link(__('Lista Usuarios'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        
    </ul>
</nav>
<div class="bitacoras index large-9 medium-8 columns content">
    <h3><?= __('Bitacoras') ?></h3>
    <table class='table table-striped' cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                
                <th scope="col"><?= $this->Paginator->sort('user_id',['Id']) ?></th>
                <th scope="col"><?= $this->Paginator->sort('email',['Usuario']) ?></th>
                <th scope="col"><?= $this->Paginator->sort('accion',['Acción']) ?></th>
                <th scope="col"><?= $this->Paginator->sort('fecha',['Fecha y hora']) ?></th>
               
            </tr>
        </thead>
        <tbody>
            <?php foreach ($bitacoras as $bitacora): ?>
            <tr>
         
                <td><?= $bitacora->has('user') ? $this->Html->link($bitacora->user->id, ['controller' => 'Users', 'action' => 'view', $bitacora->user->id]) : '' ?></td>
                <td><?= h($bitacora->email) ?></td>
                <td><?= h($bitacora->accion) ?></td>
                <td><?= h($bitacora->fecha) ?></td>
                
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
     <div class="paginator">
        <ul class="pagination">
           
            <?= $this->Paginator->prev('< ' . __('Anterior')) ?>
            <?= $this->Paginator->numbers(['before'=> '','after'=>'']) ?>
            <?= $this->Paginator->next(__('Siguiente') . ' >') ?>
           
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Pagina {{page}} de {{pages}}')]) ?></p>
    </div>
</div>
